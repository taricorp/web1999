#pragma once

#include <stdint.h>

#include <emscripten/val.h>

void gfx_Begin(emscripten::val canvasContext, emscripten::val paletteRGB,
               emscripten::val fontBitmaps);

typedef struct gfx_point_t {
    int x;
    int y;
} gfx_point_t;

void gfx_SetTextFGColor(uint8_t color);
void gfx_SetTextBGColor(uint8_t color);
void gfx_SetColor(uint8_t color);

void gfx_Rectangle(int x, int y, int w, int h);
void gfx_FillRectangle(int x, int y, int w, int h);
void gfx_HorizLine(int x, int y, int w);
void gfx_VertLine(int x, int y, int h);
void gfx_Circle(int x, int y, int radius);
void gfx_FillCircle(int x, int y, int radius);

void gfx_PrintChar(char c);
void gfx_PrintStringXY(const char *string, int x, int y);
void gfx_PrintString(const char *string);

typedef struct gfx_sprite_t {
    uint8_t width;
    uint8_t height;
    uint8_t data[];
} gfx_sprite_t;

typedef struct gfx_rletsprite_t {
    uint8_t width;
    uint8_t height;
    uint8_t data[];
} gfx_rletsprite_t;

void gfx_Sprite(const gfx_sprite_t *sprite, int x, int y);
void gfx_RLETSprite(const gfx_rletsprite_t *sprite, int x, int y);
