#include <cmath>
#include <sstream>
#include <vector>

#include <emscripten.h>
#include <emscripten/bind.h>
#include <emscripten/val.h>

#include "graphx.h"

struct RGB {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct RGBA {
    bool opaque;
    struct RGB rgb;
};

EMSCRIPTEN_BINDINGS(graphx) {
    emscripten::value_object<RGBA>("RGBA")
        .field("rgb", &RGBA::rgb)
        .field("opaque", &RGBA::opaque)
        ;

    emscripten::value_object<RGB>("RGB")
        .field("r", &RGB::r)
        .field("g", &RGB::g)
        .field("b", &RGB::b)
        ;
}

static std::vector<RGB> palette;
static emscripten::val ctx = emscripten::val::undefined();
static emscripten::val fontBitmaps = emscripten::val::undefined();
static RGBA textFGColor = {.opaque = true, .rgb = {0, 0, 0}};
static RGBA textBGColor = {.opaque = false, .rgb = {}};
static int textX;
static int textY;

static std::string getPaletteColor(uint8_t idx) {
    std::ostringstream out;
    const RGB &c = palette[idx];

    out << "rgb(" << (int) c.r << ", " << (int) c.g << ", " << (int) c.b << ")";
    return out.str();
}

void gfx_Begin(emscripten::val canvasContext, emscripten::val paletteRGB,
               emscripten::val fontBitmapsIn) {
    ctx = canvasContext;
    fontBitmaps = fontBitmapsIn;

    for (size_t i = 0; i < paletteRGB["length"].as<size_t>(); i++) {
        uint8_t r = paletteRGB[i][0].as<uint8_t>();
        uint8_t g = paletteRGB[i][1].as<uint8_t>();
        uint8_t b = paletteRGB[i][2].as<uint8_t>();

        palette.push_back({.r = r, .g = g, .b = b});
    }
}

void gfx_SetColor(uint8_t colorIndex) {
    auto color = getPaletteColor(colorIndex);
    ctx.set("fillStyle", color);
    ctx.set("strokeStyle", color);
}

void gfx_FillRectangle(int x, int y, int w, int h) {
    ctx.call<void>("fillRect", x, y, w, h);
}

void gfx_Rectangle(int x, int y, int w, int h) {
    ctx.call<void>("strokeRect", x, y, w, h);
}

static void setTextColor(struct RGBA &out, uint8_t paletteIndex) {
    if (paletteIndex == 0) {
        out = {.opaque = false, .rgb = {}};
    } else {
        out = {.opaque = true, .rgb = palette[paletteIndex]};
    }
}

void gfx_SetTextFGColor(uint8_t paletteIndex) {
    setTextColor(textFGColor, paletteIndex);
}

void gfx_SetTextBGColor(uint8_t paletteIndex) {
    setTextColor(textBGColor, paletteIndex);
}

EM_JS(int, draw_char_internal, (emscripten::EM_VAL bitmapHandle, emscripten::EM_VAL ctxHandle, int x, int y,
            bool fgOpaque, uint8_t fgRed, uint8_t fgGreen, uint8_t fgBlue,
            bool bgOpaque, uint8_t bgRed, uint8_t bgGreen, uint8_t bgBlue), {
    const bitmap = Emval.toValue(bitmapHandle);
    const charHeight = bitmap.length;
    const charWidth = bitmap[0].length;

    const ctx = Emval.toValue(ctxHandle);
    const data = ctx.getImageData(x, y, charWidth + 2, charHeight);

    for (let y = 0; y < charHeight; y++) {
        for (let x = 0; x < charWidth; x++) {
            const b = ((y * data.width) + x) * 4;
            const d = bitmap[y][x];

            if (d && fgOpaque) {
                // Draw foreground
                data.data[b] = fgRed;
                data.data[b + 1] = fgBlue;
                data.data[b + 2] = fgGreen;
                data.data[b + 3] = 255;
            } else if (!d && bgOpaque) {
                // Draw background
                data.data[b] = bgRed;
                data.data[b + 1] = bgGreen;
                data.data[b + 2] = bgBlue;
                data.data[b + 3] = 255;
            }
        }
    }

    if (bgOpaque) {
        // Fill in the space that follows
        for (let y = 0; y < charHeight; y++) {
            for (let x = charWidth; x < data.width; x++) {
                const b = ((y * data.width) + x) * 4;

                data.data[b] = bgRed;
                data.data[b + 1] = bgGreen;
                data.data[b + 2] = bgBlue;
                data.data[b + 3] = 255;
            }
        }
    }

    ctx.putImageData(data, x, y);

    return charWidth + 2;
});

void gfx_PrintChar(char c) {
    int width = draw_char_internal(
            fontBitmaps[(uint8_t)c].as_handle(), ctx.as_handle(),
            textX, textY,
            textFGColor.opaque, textFGColor.rgb.r, textFGColor.rgb.g, textFGColor.rgb.b,
            textBGColor.opaque, textBGColor.rgb.r, textBGColor.rgb.g, textBGColor.rgb.b
    );
    textX += width;
}

void gfx_PrintString(const char *s) {
    while (*s != 0) {
        gfx_PrintChar(*s);
        s += 1;
    }
}

void gfx_PrintStringXY(const char *s, int x, int y) {
    textX = x;
    textY = y;
    gfx_PrintString(s);
}

EM_JS(void, draw_sprite, (emscripten::EM_VAL ctxHandle, const RGB *paletteP, int xc, int yc, const gfx_sprite_t *data), {
    const width = HEAPU8[data];
    const height = HEAPU8[data + 1];
    const palette = HEAPU8.subarray(paletteP, paletteP + (3 * 256));

    const ctx = Emval.toValue(ctxHandle);
    const pixels = HEAPU8.subarray(data + 2, data + 2 + (width * height));
    const canvasData = new ImageData(width, height);

    for (let y = 0; y < height; y++) {
        const row = pixels.subarray(y * width, y * width + width);
        for (let x = 0; x < width; x++) {
            const base = y * width * 4 + x * 4;
            canvasData.data[base + 0] = palette[3 * row[x]];
            canvasData.data[base + 1] = palette[3 * row[x] + 1];
            canvasData.data[base + 2] = palette[3 * row[x] + 2];
            canvasData.data[base + 3] = 255;
        }
    }

    ctx.putImageData(canvasData, xc, yc);
});

void gfx_Sprite(const gfx_sprite_t *sprite, int x, int y) {
    draw_sprite(ctx.as_handle(), palette.data(), x, y, sprite);
}

EM_JS(void, draw_sprite_rlet,
      (emscripten::EM_VAL ctxHandle, const RGB *paletteP, int xc, int yc,
       const gfx_rletsprite_t *data), {
    const width = HEAPU8[data];
    const height = HEAPU8[data + 1];
    const palette = HEAPU8.subarray(paletteP, paletteP + (3 * 256));

    const ctx = Emval.toValue(ctxHandle);
    const canvasData = ctx.getImageData(xc, yc, width, height);
    let p = data + 2;

    for (let y = 0; y < height; y++) {
        let x = 0;
        while (x < width) {
            const n_transparent = HEAPU8[p++];
            x += n_transparent;
            if (x >= width) {
                break;
            }

            let n_opaque = HEAPU8[p++];
            while (n_opaque-- > 0) {
                const color = HEAPU8[p++];
                const d = 4 * y * width + 4 * x;

                canvasData.data[d] = palette[3 * color];
                canvasData.data[d + 1] = palette[3 * color + 1];
                canvasData.data[d + 2] = palette[3 * color + 2];
                canvasData.data[d + 3] = 255;
                x += 1;
            }
        }
    }

    ctx.putImageData(canvasData, xc, yc);
});

void gfx_RLETSprite(const gfx_rletsprite_t *sprite, int x, int y) {
    draw_sprite_rlet(ctx.as_handle(), palette.data(), x, y, sprite);
}

void gfx_HorizLine(int x, int y, int width) {
    ctx.call<void>("moveTo", x, y);
    ctx.call<void>("lineTo", x + width, y);
    ctx.call<void>("stroke");
}

void gfx_VertLine(int x, int y, int height) {
    ctx.call<void>("moveto", x, y);
    ctx.call<void>("lineTo", x, y + height);
    ctx.call<void>("stroke");
}

void gfx_Circle(int x, int y, int radius) {
    ctx.call<void>("arc", x, y, radius, 0, 2 * M_PI);
    ctx.call<void>("stroke");
}

void gfx_FillCircle(int x, int y, int radius) {
    ctx.call<void>("arc", x, y, radius, 0, 2 * M_PI);
    ctx.call<void>("fill");
}
