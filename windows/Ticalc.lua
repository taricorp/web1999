require 'w9bcc'

local winHeight = 109
local navWidth = 88
local headerHeight = 16

WindowTitle("ticalc.org")
WindowSize{width = 240, height = winHeight}
FillWindow('white')

SetDrawColor('red')
FillRectangle{x = 0, y = 0, width = navWidth, height = headerHeight}
SetTextColor('yellow')
DrawText{x = 8, y = 4, text = "ticalc.org"}

SetDrawColor('yellow')
FillRectangle{x = 0, y = headerHeight, width = navWidth, height = winHeight - headerHeight}

SetDrawColor('black')
SetTextColor('black')
local navLinks = {
    "Calculators",
    "Programming",
    "Linking",
    "Archives",
    "Features",
    "News & Lists",
    "FAQ",
    "Other Sites",
}
for i, link in ipairs(navLinks) do
    local yo = 11 * (i - 1)
	DrawHorizontalLine{x = 0, y = headerHeight + yo, width = navWidth}
	DrawText{x = 2, y = headerHeight + 2 + yo, text = link}
end
DrawHorizontalLine{x = 0, y = headerHeight + (11 * #navLinks), width = navWidth}
DrawVerticalLine{x = navWidth, y = 0, height = winHeight}

local bodyText = {
    "Your Internet",
    "headquarters for",
    "Texas Instruments'",
    "graphing calculators.",
    "",
    "Home of ZShell and",
    "Fargo for the TI-85",
    "and TI-92, plus a",
    "comprehensive",
    "archive of programs",
    "and utilities.",
}
for i, line in ipairs(bodyText) do
    local yo = (i - 1) * 9
	if #line > 0 then
		DrawText{x = navWidth + 3, y = 2 + yo, text = line}
	end
end
