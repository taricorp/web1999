require 'w9bcc'

local WW = 168
WindowTitle("Singles near you!")
WindowSize{width = 168, height = 105}

FillWindow('white')

SetDrawColor('silver')
DrawRectangle{x = 0, y = 0, width = 96, height = 64}
local broken_image = Sprite:load('broken_image.png')
broken_image:draw_at{x = (96 / 2) - (broken_image.width / 2),
                     y = (64 / 2) - (broken_image.height / 2)}

local wordart_left = Sprite:load('hotsingles_left.png')
local wordart_right = Sprite:load('hotsingles_right.png')
local wordart_top = 66
local wordart_width = wordart_left.width + wordart_right.width + 10;
wordart_left:draw_at{x = (WW - wordart_width) / 2,
                     y = wordart_top}
wordart_right:draw_at{x = WW - ((WW - wordart_width) / 2) - wordart_right.width,
                      y = wordart_top}

DrawText{x = 10, y = 94, text = "Waiting to meet today!"}
SetTextColor{fg = 'green'}
DrawText{x = 118, y = 22, text = "CHAT"}
DrawText{x = 120, y = 30, text = "NOW!"}
