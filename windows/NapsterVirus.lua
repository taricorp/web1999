--[[
LimeWire didn't arrive until early 2000, but as a vague feeling malware doesn't seem
to have been nearly as common on Napster as it was on Napster's immediate successors
(including LimeWire).

"Crawling" wasn't released until October of 2000, so it's likely to have been widely
available on LimeWire especially since it was a top-40 single in many countries.
]]

require 'w9bcc'

WindowTitle('LimeWire')
local window_size = {
    width = 158,
    height = 47,
}
WindowSize(window_size)

FillWindow('white')

local lines = {
    {y = 2, text = "Linkin Park - Crawling"},
    {y = 14, text = "Size: 3,191 KB"},
    {y = 24, text = "Type: exe"},
}
for _, line in ipairs(lines) do
    line.x = 2
    DrawText(line)
end

SetDrawColor('black')
DrawHorizontalLine{x = 0, y = 11, width = window_size.width}

SetTextColor{'blue'}
DrawText{x = 52, y = 35, text = "Download"}
SetDrawColor('blue')
DrawHorizontalLine{x = 52, y = 43, width = 60}
