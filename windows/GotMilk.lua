require 'w9bcc'

WindowTitle("Got milk?")
WindowSize{width = 100, height = 105}

FillWindow('blue')

Sprite:load('gotmilk.png'):draw_at{x = 40, y = 4}

SetTextColor('white')
DrawText{x = 4, y = 50, text = "GOT"}
DrawText{x = 4, y = 62, text = "MILK?"}
