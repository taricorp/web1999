package.path = "../?.lua;" .. package.path
require 'w9bcc'

local image = Sprite:load('allyourbase.png')

WindowTitle("ALL YOUR BASE")
WindowSize{width = image.width, height = image.height}

-- The image has its background zeroed out in the regions that are constant colors,
-- orange on the top and black on the bottom. Fill that in "manually" so the image
-- can be stored more efficiently (with RLET).
SetDrawColor('#ffb600')
FillRectangle{x = 0, y = 0, width = 240, height = 80}
SetDrawColor('black')
FillRectangle{x = 0, y = 80, width = 240, height = 80}

image:draw_at{0, 0}
