require 'w9bcc'

WindowTitle("YOU WON!")
WindowSize{width = 108, height = 77}

FillWindow('white')

Sprite:load('sportscar.png'):draw_at{x = 12, y = 24}

SetTextColor('maroon')
DrawText{x = 3, y = 2, text = "You are visitor"}
SetTextColor('red')
DrawText{x = 17, y = 12, text = "#1,000,000!"}
SetDrawColor('green')
FillRectangle{x = 4, y = 60, width = 100, height = 15}
SetTextColor('white')
DrawText{x = 13, y = 64, text = "CLAIM PRIZE"}
