package.path = "../?.lua;" .. package.path
require 'w9bcc'

local window_size = {
    width = 158,
    height = 129,
}
WindowSize(window_size)
WindowTitle("Strong Bad Sings")

-- Color bars of a picturesque desert sunset.
local bars = {
    {'#ffcc00', 25},
    {'#d47800', 15},
    {'#b75b00', 17},
    {'#891d00', 33},
    {'black', 39}
}
local ry = 0
for _, bar in ipairs(bars) do
    local height = bar[2]
    SetDrawColor(bar[1])
    FillRectangle{x = 0, y = ry, width = window_size.width, height = height}
    ry = ry + height
end
assert(ry == window_size.height, "Combined bar height should equal window size")

-- The sun, low in the sky
SetDrawColor('#cc9933')
FillCircle{x = 119, y = 73, r = 12}

-- Strong Bad himself!
local strongbad = Sprite:load('strongbad.png')
strongbad:draw_at{x = 0, y = 10}

SetTextColor('red')
DrawText{x = 80, y = 4, text = "STRONG BAD"}
DrawText{x = 96, y = 14, text = "SINGS"}
-- Crossed out price
SetTextColor('green')
DrawText{x = 96, y = 32, text = "$99.99?"}
SetDrawColor('green')
DrawHorizontalLine{x = 96, y = 35, width = 50}
-- Actual price
SetTextColor('lime')
DrawText{x = 90, y = 44, text = "$193.75!!!"}
-- Phone number
SetTextColor('white')
DrawText{x = 86, y = 108, text = "1-800-"}
DrawText{x = 64, y = 118, text = "555-SBSINGS"}