package.path = "../?.lua;" .. package.path
require 'w9bcc'

WindowTitle("Despair")
WindowSize{width = 198, height = 159}
FillWindow('black')

--[[
This particular design ("Mistakes") was on sale on Despair's web site at least as
early as April 1999. It's been modified a bit to work better on a small, low-res
screen.
]]

local illustration = Sprite:load('mistakes_illus.png')
illustration:draw_at{x = 19, y = 10}

SetDrawColor('teal')
FillRectangle{x = 66, y = 120, width = 66, height = 11}
DrawHorizontalLine{x = 66, y = 133, width = 66}
-- Default foreground color makes this black on cyan
DrawText{x = 68, y = 123, text = "MISTAKES"}

SetTextColor{'white'}
DrawText{x = 15, y = 137, text = "Your purpose may only be"}
DrawText{x = 2, y = 147, text = "to act as a warning to others"}
