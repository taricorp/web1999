local _w9bcc = require '_w9bcc'

local function emit_u8(value)
    assert(value & 0xff == value)
    _w9bcc.emit_byte(value)
end

local function emit_u16(value)
    assert(value & 0xffff == value)
    emit_u8(value & 0xff)
    emit_u8((value >> 8) & 0xff)
end

local function emit_u9x2(x1, x2)
    emit_u8(x1 & 0xff)
    emit_u8(x2 & 0xff)
    emit_u8(((x1 >> 8) & 1) | ((x2 >> 7) & 2))
end

local function emit_bytes(value)
    for _, byte in ipairs(value) do
        emit_u8(byte)
    end
end

local Bytecode = {
    FILL = 0,
    COLOR = 1,
    RECT = 2,
    RECT_FILL = 3,
    HORIZ_LINE = 4,
    VERT_LINE = 5,
    CIRCLE = 6,
    CIRCLE_FILL = 7,
    TEXT_FG_COLOR = 0x10,
    TEXT_COLORS = 0x11,
    STR_XY = 0x12,
    STR = 0x13,
    SPRITE = 0x20,
    SPRITE_RLET = 0x21,
    EOF = 0xff,
}

Sprite = {}
Sprite.__index = Sprite

function Sprite:new(data, width, height, is_transparent, transparent_color)
    return setmetatable({
        data = data,
        width = width,
        height = height,
        is_transparent = is_transparent,
        transparent_color = transparent_color,
    }, self)
end

function Sprite:load(path, dithering)
    if dithering == nil then
    	dithering = 0
    end
    assert(type(dithering) == 'number' and dithering >= 0 and dithering <= 1,
           "Dithering quality must be a number in the range 0 to 1")

    local s = _w9bcc.load_sprite(path, dithering)
    return Sprite:new(s.data, s.width, s.height, s.is_transparent, s.transparent_color)
end

function Sprite:from_raw_data(width, height, data, transparent_color)
    assert(#data == (width * height), string.format("Sprite data must be exactly width * height bytes: wanted %d bytes, got %d", width * height, #data))

    local any_transparent = false
    for i, datum in ipairs(data) do
        local byte = math.tointeger(datum)
        if byte == nil or byte < 0 or byte > 255 then
            error(string.format("Sprite data must be 8-bit integers: value at index %d (%s) is not valid", i, datum))
        end
        if byte == transparent_color then
            any_transparent = true
        end
    end
    return Sprite:new(data, width, height, any_transparent, transparent_color)
end

function Sprite:draw_at(location)
    local x = location[1] or location.x
    local y = location[2] or location.y

    if self.is_transparent then
        emit_u8(Bytecode.SPRITE_RLET)
    else
        emit_u8(Bytecode.SPRITE)
    end
    emit_u16(x)
    emit_u8(y)

    if self.is_transparent then
        --[[ RLET data format:
             * width
             * height
             * for each row:
               * count of transparent pixels
               * count of opaque pixels, followed by literal bytes
               * repeat pair of counts until end of row ]]
        local data_out = {}
        for row = 0, self.height - 1 do
            local row_start = (row * self.width) + 1
            local row_end = row_start + self.width

            while row_start < row_end do
                local n_transparent = 0
                while self.data[row_start] == self.transparent_color and row_start < row_end do
                    n_transparent = n_transparent + 1
                    row_start = row_start + 1
                end
                table.insert(data_out, n_transparent)

                local n_opaque = 0
                local opaque_start = row_start
                while self.data[row_start] ~= self.transparent_color and row_start < row_end do
                    n_opaque = n_opaque + 1
                    row_start = row_start + 1
                end
                if n_opaque > 0 then
                    table.insert(data_out, n_opaque)
                    for i = opaque_start, row_start - 1 do
                        table.insert(data_out, self.data[i])
                    end
                end
            end
        end

        -- RLET sprites must have their size noted because it's not strictly related
        -- to the dimensions that are included.
        emit_u16(#data_out + 2)
        emit_u8(self.width)
        emit_u8(self.height)
        emit_bytes(data_out)
    else
        -- Plain (non-RLET) sprites have size trivially computed from width * height.
        emit_u8(self.width)
        emit_u8(self.height)
        emit_bytes(self.data)
    end
end

function WindowSize(dimensions)
    local width = dimensions[1] or dimensions.width
    local height = dimensions[2] or dimensions.height
    _w9bcc.set_window_size(width, height)
end

function WindowTitle(title)
    _w9bcc.set_window_title(title)
end

function FillWindow(color)
    emit_u8(Bytecode.FILL)
    emit_u8(_w9bcc.quantize_color(color))
end

function SetDrawColor(color)
    emit_u8(Bytecode.COLOR)
    emit_u8(_w9bcc.quantize_color(color))
end

local function _DrawRectangle(bytecode, p)
    local x = p.x or p[1]
    local y = p.y or p[2]
    local width = p.width or p[3]
    local height = p.height or p[4]

    emit_u8(bytecode)
    emit_u9x2(x, width)
    emit_u8(y)
    emit_u8(height)
end

function DrawRectangle(p)
    _DrawRectangle(Bytecode.RECT, p)
end

function FillRectangle(p)
    _DrawRectangle(Bytecode.RECT_FILL, p)
end

function SetTextColor(c)
    local fg, bg = nil, nil
    if type(c) == 'table' then
        fg = c[1] or c.fg
        bg = c[2] or c.bg
    else
        fg = c
    end

    if bg ~= nil then
        emit_bytes{Bytecode.TEXT_COLORS, _w9bcc.quantize_color(fg), _w9bcc.quantize_color(bg)}
    else
        emit_bytes{Bytecode.TEXT_FG_COLOR, _w9bcc.quantize_color(fg)}
    end
end

function DrawText(text)
    local str = text[1] or text.text
    local x = text[2] or text.x
    local y = text[3] or text.y

    if (x ~= nil or y ~= nil) then
        emit_u8(Bytecode.STR_XY)
        emit_u16(x)
        emit_u8(y)
    else
        emit_u8(Bytecode.STR)
    end

    for i = 1, #str do
        local byte = string.byte(str, i)
        assert(byte ~= 0, "Strings may not contain null bytes")
        emit_u8(byte)
    end
    emit_u8(0)
end

function DrawHorizontalLine(l)
    local x = l[1] or l.x
    local y = l[2] or l.y
    local width = l[3] or l.width

    emit_u8(Bytecode.HORIZ_LINE)
    emit_u9x2(x, width)
    emit_u8(y)
end

function DrawVerticalLine(l)
    local x = l[1] or l.x
    local y = l[2] or l.y
    local height = l[3] or l.height

    emit_u8(Bytecode.VERT_LINE)
    emit_u9x2(x, height)
    emit_u8(y)
end

function FillCircle(c)
    local x = c[1] or c.x
    local y = c[2] or c.y
    local r = c[3] or c.r

    emit_u8(Bytecode.CIRCLE_FILL)
    emit_u16(x)
    emit_u8(y)
    emit_u8(r)
end