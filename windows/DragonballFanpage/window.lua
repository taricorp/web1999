package.path = "../?.lua;" .. package.path
require 'w9bcc'

WindowTitle("Dragonball oasis")
WindowSize{width = 178, height = 159}
FillWindow('white')

local goku = Sprite:load('chibi_goku_80px_web.png')
local vegeta = Sprite:load('chibi_vegeta_80px_web.png')
goku:draw_at{x = 0, y = 3}
vegeta:draw_at{x = 110, y = 74}

-- Lightly optimized: black text draws before other colors to save
-- setting the color again.
DrawText{x = 10, y = 95, text = "Welcome to my"}
DrawText{x = 24, y = 117, text = "fan page!"}

-- Between the previous two lines, a string in rainbow colors
DrawText{x = 10, y = 106, text = ""}
local rainbow = "DRAGONBALL Z"
for i = 1, #rainbow do
    SetTextColor{fg = string.format("hsl(%d, 75%%, 50%%)", 43 * (i - 1))}
    DrawText{text = rainbow:sub(i, i)}
end

-- Decorative box containing the site title
SetDrawColor('#ff7800')
DrawHorizontalLine{x = 88, y = 16, width = 84}
FillRectangle{x = 88, y = 18, width = 84, height = 44}
DrawHorizontalLine{x = 88, y = 18 + 44 + 2, width = 84}
SetTextColor{fg = 'white'}
DrawText{x = 91, y = 30, text = "DRAGONBALL"}
DrawText{x = 110, y = 42, text = "OASIS"}

-- This site is part of a webring!
SetDrawColor('silver')
DrawRectangle{x = 8, y = 134, width = 89, height = 21}
SetDrawColor('olive')
FillRectangle{x = 9, y = 135, width = 87, height = 10}
-- Still white foreground from above
DrawText{x = 13, y = 136, text = "DBZ webring"}
SetTextColor{fg = 'teal'}
DrawText{x = 11, y = 146, text = "<Prev   Next>"}
