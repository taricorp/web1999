package.path = "../?.lua;" .. package.path
require 'w9bcc'

WindowTitle("America Online")
WindowSize{width = 157, height = 69}

FillWindow('white')
Sprite:load('aol_logo.png'):draw_at{x = 2, y = 2}

DrawText{x = 50, y = 6, text = "540 hours free!"}
SetDrawColor('black')
DrawHorizontalLine{x = 121, y = 14, width = 33}

SetTextColor('purple')
DrawText{x = 91, y = 18, text = "New"}
DrawText{x = 64, y = 27, text = "version 6.0!"}

SetTextColor('maroon')
DrawText{x = 88, y = 38, text = "Blazing"}
DrawText{x = 74, y = 47, text = "56k speeds!"}

SetTextColor('blue')
SetDrawColor('blue')
DrawText{x = 36, y = 58, text = "Join today!"}
DrawHorizontalLine{x = 36, y = 67, width = 73}
