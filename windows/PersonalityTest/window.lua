package.path = "../?.lua;" .. package.path
require 'w9bcc'

WindowTitle("What droid?")
WindowSize{width = 168, height = 105}

FillWindow('white')
Sprite:load('r2unit.png'):draw_at{x = 2, y = 2}

SetDrawColor('yellow')
FillRectangle{x = 70, y = 46, width = 90, height = 34}
-- Black text drawn first to save needing to reset text color
DrawText{x = 98, y = 47, text = "Free"}
DrawText{x = 74, y = 58, text = "personality"}
DrawText{x = 96, y = 68, text = "test!"}

SetTextColor('maroon')
DrawText{x = 72, y = 5, text = "What kind of"}
DrawText{x = 86, y = 25, text = "are you?"}
SetTextColor('blue')
DrawText{x = 94, y = 15, text = "DROID"}

SetTextColor('olive')
DrawText{x = 4, y = 96, text = "Now including Episode I!"}