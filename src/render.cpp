#ifndef __EMSCRIPTEN__
#include <debug.h>
#endif
#include <graphx.h>

#include "bytecode.hpp"
#include "colors.h"
#include "render.hpp"

#ifdef __EMSCRIPTEN__
Renderer::Renderer(emscripten::val canvasElement, emscripten::val paletteValues,
                   emscripten::val fontBitmaps)
    : x(0),
      y(0),
      w(canvasElement["width"].as<uint16_t>()),
      h(canvasElement["height"].as<uint8_t>()) {
    using namespace std::string_literals;
    auto ctx = canvasElement.call<emscripten::val>("getContext", "2d"s);

    gfx_Begin(ctx, paletteValues, fontBitmaps);
}

void Renderer::renderJS(const emscripten::val &bytecode) {
    std::vector<uint8_t> bytes =
            emscripten::convertJSArrayToNumberVector<uint8_t>(bytecode);
    if (bytes.size() == 0 || bytes.back() != OP_EOF) {
        EM_ASM({throw new Error('Bytecode must end with OP_EOF, but did not')});
    }

    render(bytes.data());
}

EMSCRIPTEN_BINDINGS(Renderer) {
    emscripten::class_<Renderer>("Renderer")
            .constructor<emscripten::val, emscripten::val, emscripten::val>()
            .function("render", &Renderer::renderJS);
}

#else /* !__EMSCRIPTEN__ */
Renderer::Renderer(Rect contentArea)
    : x(contentArea.x),
      y(contentArea.y),
      w(contentArea.width),
      h(contentArea.height) {}
#endif

void Renderer::renderOne(Command command) {
#ifndef __EMSCRIPTEN__
    dbg_printf("Render command op %d\n", command.op);
#endif

    switch (command.op) {
        case OP_EOF:
            break;

        case OP_FILL:
            gfx_SetColor(command.color);
            gfx_FillRectangle(x, y, w, h);
            break;

        case OP_COLOR:
            gfx_SetColor(command.color);
            break;

        case OP_RECT:
            gfx_Rectangle(x + command.rect.x, y + command.rect.y, command.rect.w, command.rect.h);
            break;

        case OP_RECT_FILL:
            gfx_FillRectangle(x + command.rect.x, y + command.rect.y, command.rect.w, command.rect.h);
            break;

        case OP_HORIZ_LINE:
            gfx_HorizLine(x + command.line.x, y + command.line.y, command.line.length);
            break;

        case OP_VERT_LINE:
            gfx_VertLine(x + command.line.x, y + command.line.y, command.line.length);
            break;

        case OP_CIRCLE:
            gfx_Circle(x + command.circle.x, y + command.circle.y, command.circle.r);
            break;

        case OP_CIRCLE_FILL:
            gfx_FillCircle(x + command.circle.x, y + command.circle.y, command.circle.r);
            break;

        case OP_TEXT_FG_COLOR:
            gfx_SetTextFGColor(command.color);
            break;

        case OP_TEXT_COLORS:
            gfx_SetTextFGColor(command.text_colors.fg);
            gfx_SetTextBGColor(command.text_colors.bg);
            break;

        case OP_STR_XY:
            gfx_PrintStringXY(command.str_xy.str, x + command.str_xy.x, y + command.str_xy.y);
            break;

        case OP_STR:
            gfx_PrintString(command.str);
            break;

        case OP_SPRITE:
            gfx_Sprite((const gfx_sprite_t *)command.sprite.sprite_data, x + command.sprite.x, y + command.sprite.y);
            break;

        case OP_SPRITE_RLET:
            gfx_RLETSprite((const gfx_rletsprite_t *)command.sprite_rlet.sprite_data,
                           x + command.sprite_rlet.x, y + command.sprite_rlet.y);
            break;

        default:
#ifdef __EMSCRIPTEN__
            EM_ASM({
                throw new Error('Bytecode command ' + $0 + ' is not implemented')
            }, command.op);
#else
            dbg_printf("Ignoring unknown bytecode operation %d\n", command.op);
            break;
#endif
    }
}

void Renderer::render(const uint8_t *bytecode) {
    BytecodeDecoder decoder(bytecode);
    Command cmd;

    // Set default colors and modes
    gfx_SetTextFGColor(C_BLACK);
    gfx_SetTextBGColor(0);

    do {
        cmd = decoder.next();
        renderOne(cmd);
    } while (cmd.op != OP_EOF);
}
