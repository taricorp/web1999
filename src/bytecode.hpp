#pragma once

#include <cstdint>

enum Operation {
    /** Fill the window with a chosen palette index. */
    OP_FILL = 0,
    /**
     * Set the palette index with which graphics commands will draw
     * until changed with another color command.
     */
    OP_COLOR = 1,
    /**
     * Draw an unfilled rectangle (outlined) with chosen width and height with
     * the top left corner at chosen X and Y coordinates.
     */
    OP_RECT = 2,
    /**
     * Draw a filled rectangle. Same as OP_RECT, but the interior is filled
     * with the same color as the outline.
     */
    OP_RECT_FILL = 3,
    /**
     * Draw a horizontal line start at chosen X and Y coordinates with a
     * specified width.
     */
    OP_HORIZ_LINE = 4,
    /** Vertical line at X, Y with specified length. */
    OP_VERT_LINE = 5,
    /** Unfilled circle centered at X, Y with specified radius. */
    OP_CIRCLE = 6,
    /** Filled circle X, Y, r. */
    OP_CIRCLE_FILL = 7,

    /** Set the foreground text color to a palette index. */
    OP_TEXT_FG_COLOR = 0x10,
    /** Set the foreground and background text colors to palette indexes. */
    OP_TEXT_COLORS = 0x11,
    /** Display a string at specified X and Y coordinates. */
    OP_STR_XY = 0x12,
    /** Display a string immediately following the last displayed text. */
    OP_STR = 0x13,

    OP_SPRITE = 0x20,
    OP_SPRITE_RLET = 0x21,

    OP_EOF = 0xff,
};

struct Command {
    Operation op;
    union {
        // FILL, COLOR, TEXT_FG_COLOR
        uint8_t color;
        // TEXT_COLORS
        struct {
            uint8_t fg;
            uint8_t bg;
        } text_colors;
        // RECT, RECT_FILL
        struct {
            uint16_t x;
            uint8_t y;
            uint16_t w;
            uint8_t h;
        } rect;
        // HORIZ_LINE, VERT_LINE
        struct {
            uint16_t x;
            uint8_t y;
            uint16_t length;
        } line;
        // CIRCLE, CIRCLE_FILL
        struct {
            uint16_t x;
            uint8_t y;
            uint8_t r;
        } circle;
        // STR_XY
        struct {
            uint16_t x;
            uint8_t y;
            const char *str;
        } str_xy;
        // STR
        const char *str;
        // SPRITE
        struct {
            uint16_t x;
            uint8_t y;
            const void *sprite_data;
        } sprite;
        // SPRITE_RLET
        struct {
            uint16_t x;
            uint8_t y;
            const void *sprite_data;
        } sprite_rlet;
    };
};

class BytecodeDecoder {
    const uint8_t *p;

    inline uint8_t next_u8() {
        return *p++;
    }

    inline uint16_t next_u16() {
        uint16_t out = *p++;
        out |= (*p++) << 8;
        return out;
    }

    struct xw {
        uint16_t x;
        uint16_t w;
    };

    inline struct xw next_u9x2() {
        uint16_t x = next_u8();
        uint16_t w = next_u8();
        uint8_t msbs = next_u8();
        x |= (msbs & 1) << 8;
        w |= (msbs & 2) << 7;
        return (struct xw){ .x = x, .w = w };
    }

public:
    BytecodeDecoder(const uint8_t *p);
    Command next();
};
