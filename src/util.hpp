#pragma once

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) < (b)) ? (b) : (a))

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))
