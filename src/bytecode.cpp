#include "bytecode.hpp"

BytecodeDecoder::BytecodeDecoder(const uint8_t *p) : p(p) {}

Command BytecodeDecoder::next() {
    Operation op = (Operation)next_u8();
    switch (op) {
        case OP_EOF:
        default:
        {
            Command out = {};
            out.op = op;
            return out;
        }

        case OP_FILL:
        case OP_COLOR:
        case OP_TEXT_FG_COLOR:
            return {
                .op = op,
                .color = next_u8(),
            };

        case OP_TEXT_COLORS:
        {
            auto fg = next_u8();
            auto bg = next_u8();
            return {
                .op = op,
                .text_colors = {
                    .fg = fg,
                    .bg = bg,
                }
            };
        }

        case OP_RECT:
        case OP_RECT_FILL:
        {
            auto xw = next_u9x2();
            auto y = next_u8();
            auto h = next_u8();
            return {
                    .op = op,
                    .rect = {
                            .x = xw.x,
                            .y = y,
                            .w = xw.w,
                            .h = h,
                    },
            };
        }

        case OP_HORIZ_LINE:
        case OP_VERT_LINE:
        {
            auto xl = next_u9x2();
            auto y = next_u8();
            return {
                .op = op,
                .line = {
                    .x = xl.x,
                    .y = y,
                    .length = xl.w,
                },
            };
        }

        case OP_CIRCLE:
        case OP_CIRCLE_FILL:
        {
            auto x = next_u16();
            auto y = next_u8();
            auto r = next_u8();
            return {
                .op = op,
                .circle = {
                    .x = x,
                    .y = y,
                    .r = r,
                },
            };
        }

        case OP_STR_XY:
        {
            auto x = next_u16();
            auto y = next_u8();
            auto str = (const char *)p;
            while (*p++ != 0) {
                // Advance p past the string contents
            }
            return {
                    .op = op,
                    .str_xy = {
                            .x = x,
                            .y = y,
                            .str = str,
                    },
            };
        }

        case OP_STR:
        {
            auto str = (const char *)p;
            while (*p++ != 0) {
                // Advance p past the string contents
            }
            return {
                .op = op,
                .str = str,
            };
        }

        case OP_SPRITE:
        {
            auto x = next_u16();
            auto y = next_u8();
            const void *data = p;
            auto w = next_u8();
            auto h = next_u8();
            p += w * h;
            return {
                    .op = op,
                    .sprite = {
                            .x = x,
                            .y = y,
                            .sprite_data = data,
                    },
            };
        }

        case OP_SPRITE_RLET:
        {
            auto x = next_u16();
            auto y = next_u8();
            auto data_len = next_u16();
            auto data = p;
            p += data_len;
            return {
                .op = op,
                .sprite_rlet = {
                    .x = x,
                    .y = y,
                    .sprite_data = data,
                }
            };
        }
    }
};
