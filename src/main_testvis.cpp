#include <debug.h>
#include <sys/timers.h>
#include <ti/getcsc.h>

#include "colors.h"
#include "credits.hpp"
#include "window.hpp"

#include "bytecode.hpp"
#include "bytecode_window.hpp"
#include "gfx/gfx.h"

#define WINTYPE(name) extern const struct WindowDescriptor name;
#include "../windows/windows.h"
#undef WINTYPE

static const struct {
    const char *name;
    const struct WindowDescriptor *descriptor;
} MODES[] = {
#define WINTYPE(name) {#name, &name},
#include "../windows/windows.h"
#undef WINTYPE
};

static const unsigned N_MODES = sizeof(MODES) / sizeof(*MODES);

void testvis_main(void) {

    unsigned modeIdx = 0;
    while (1) {
        gfx_ZeroScreen();
        dbg_printf("MODE %d/%d: %s\n", modeIdx, N_MODES - 1, MODES[modeIdx].name);
        BytecodeWindow(160, 120, MODES[modeIdx].descriptor).draw(true);
        gfx_BlitBuffer();

        int key;
        while ((key = os_GetCSC()) == 0) {
            msleep(30);
        }
        if (key == sk_Left) {
            if (modeIdx > 0) {
                modeIdx -= 1;
            } else {
                modeIdx = N_MODES - 1;
            }
        } else if (key == sk_Right) {
            modeIdx += 1;
            if (modeIdx >= N_MODES) {
                modeIdx = 0;
            }
        } else {
            break;
        }
    }
}
