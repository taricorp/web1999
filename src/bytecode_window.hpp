#pragma once

#include "window.hpp"

class BytecodeWindow : public Window {
    const uint8_t *script;

public:
    BytecodeWindow(int x, uint8_t y, const WindowDescriptor *d);

    void draw(bool active) const override;
};
