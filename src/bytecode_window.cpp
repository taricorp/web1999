#include "bytecode_window.hpp"

BytecodeWindow::BytecodeWindow(int x, uint8_t y, const WindowDescriptor *d) :
    Window(x, y, d->content_width, d->content_height, d->title), script(d->script) {
}

void BytecodeWindow::draw(bool active) const {
    Window::draw(active);

    Renderer renderer(getContentArea());
    renderer.render(script);
}