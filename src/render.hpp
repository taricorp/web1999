#pragma once

#include <stdint.h>

#ifdef __EMSCRIPTEN__
#include <string>

#include <emscripten.h>
#include <emscripten/bind.h>
#include <emscripten/val.h>
#endif

#include "rect.hpp"

class Renderer {
    int x, y, w, h;
    void renderOne(Command command);

public:
#ifdef __EMSCRIPTEN__
    Renderer(emscripten::val canvasElement, emscripten::val paletteValues,
             emscripten::val fontBitmaps);
    void renderJS(const emscripten::val &bytecode);
#else
    Renderer(Rect contentArea);
#endif
    void render(const uint8_t *bytecode);
};
