#pragma once

#include <graphx.h>
#include <stdint.h>

#include "bytecode.hpp"
#include "rect.hpp"
#include "render.hpp"

struct WindowDescriptor {
    uint16_t content_width;
    uint8_t content_height;
    const char *title;
    const uint8_t *script;
};

class Window {
private:
    Rect location;
    const char *title;

    static const uint8_t TITLEBAR_HEIGHT = 9;

protected:
    /**
     * Create a window with the given width and height in pixels, approximately
     * centered at the screen coordinates (x, y).
     */
    Window(int x, uint8_t y, uint16_t width, uint16_t height, const char *title);

public:
    virtual ~Window() = default;
    virtual void draw(bool active) const = 0;

    Rect getBounds() const;

    /**
     * Return the rectangle bounding the window's content area.
     */
    Rect getContentArea() const;

    /**
     * Return the bounding rectangle for clicking the close button.
     */
    Rect getCloseTarget() const;

    /**
     * Return the bounding rectangle for dragging the titlebar to move the window.
     */
    Rect getMoveTarget() const;

    void shift(gfx_point_t shift);
};
