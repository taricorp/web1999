static SCRIPT: &'static str = r#"
require 'w9bcc'

cursor = Sprite:from_raw_data(7, 5, {
    5, 0, 0, 0, 0, 0, 0,
    5, 5, 0, 0, 0, 0, 0,
    5, 2, 5, 0, 0, 0, 0,
    5, 2, 2, 5, 0, 0, 0,
    0, 5, 2, 2, 5, 5, 5,
}, 0)

assert(cursor.width == 7)
assert(cursor.height == 5)
assert(cursor.is_transparent == true)
assert(cursor.transparent_color == 0)

cursor:draw_at{0, 0}
"#;

#[test]
fn test_rlet_sprite() {
    std::env::set_var("LUA_PATH", format!("{}/../windows/?.lua", env!("CARGO_MANIFEST_DIR")));
    let bc = w99bcc::execute(SCRIPT).expect("Should execute without error");

    assert_eq!(
        &bc.codes,
        &[
            // SPRITE_RLET at (0, 0)
            0x21, 0, 0, 0, // 32 bytes of data
            32, 0, // 7 x 5 pixels
            7, 5, // Data in rows
            0, 1, 5, 6, 0, 2, 5, 5, 5, 0, 3, 5, 2, 5, 4, 0, 4, 5, 2, 2, 5, 3, 1, 6, 5, 2, 2, 5, 5,
            5, // EOF bytecode
            255
        ],
        "RLET sprite bytecode didn't match expectations"
    );
}
