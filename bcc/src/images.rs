//! Image handling for Lua.

use std::path::Path;

use mlua::prelude::*;
use mlua::{ErrorContext, FromLua, IntoLua, Lua, Value as LuaValue};

use crate::CompileError;

/// Newtype for colors as palette locations.
///
/// This is primarily useful to get FromLua and IntoLua conversions.
pub struct PaletteIndex(u8);

impl PaletteIndex {
    const TRANSPARENT: PaletteIndex = PaletteIndex(0);
}

/// Translate colors in various formats into palette indexes.
///
/// Accepts the following formats:
///  * Strings, interpreted as CSS colors (like 'blue' or '#fdcb12').
///  * Tables of 3 or 4 elements, containing 8-bit values for the RGB and
///    optionally alpha channel.
///  * Integers, treated as raw 8-bit palette indexes.
impl<'lua> FromLua<'lua> for PaletteIndex {
    fn from_lua(value: LuaValue<'lua>, lua: &'lua Lua) -> mlua::Result<Self> {
        match value {
            // Strings are parsed as CSS colors
            LuaValue::String(s) => match csscolorparser::parse(s.to_str()?) {
                Ok(c) => Ok(lua.quantize_color(c.to_rgba8())),
                Err(e) => Err(mlua::Error::FromLuaConversionError {
                    from: "string",
                    to: "Palette index",
                    message: Some(e.to_string()),
                }),
            },
            // Tables are treated as 8-bit RGB[A] channel values
            LuaValue::Table(table) => {
                let mut channels = [255u8; 4];
                let n_channels = table.len()?;
                if !(3..=4).contains(&n_channels) {
                    return Err(mlua::Error::FromLuaConversionError {
                        from: "table",
                        to: "Palette index",
                        message: Some(format!(
                            "Table must have length 3 or 4 to convert to color, but has length {}",
                            n_channels
                        )),
                    });
                }

                for i in 1..=n_channels {
                    channels[i as usize - 1] = table
                        .get(i)
                        .map_err(|e| e.context("Color values must be integers in range 0 - 255"))?;
                }

                Ok(lua.quantize_color(channels))
            }
            LuaValue::Integer(x) => match u8::try_from(x) {
                Ok(x) => Ok(PaletteIndex(x)),
                Err(_) => Err(mlua::Error::FromLuaConversionError {
                    from: "integer",
                    to: "Palette index",
                    message: Some(format!(
                        "Integer colors must be in range 0 - 255, but got {}",
                        x
                    )),
                }),
            },
            // Other types are unsupported
            val => Err(mlua::Error::RuntimeError(format!(
                "{:?} cannot be interpreted as a color (must be string, table, or integer",
                val
            ))),
        }
    }
}

impl IntoLua<'_> for PaletteIndex {
    fn into_lua(self, _: &Lua) -> LuaResult<LuaValue> {
        Ok(LuaValue::Integer(self.0.into()))
    }
}

pub struct Sprite {
    is_transparent: bool,
    width: u8,
    height: u8,
    data: Vec<u8>,
}

impl mlua::UserData for Sprite {
    fn add_fields<'lua, F: mlua::UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field("transparent_color", PaletteIndex::TRANSPARENT.0);
        fields.add_field_method_get("width", |_lua, sprite| Ok(sprite.width));
        fields.add_field_method_get("height", |_lua, sprite| Ok(sprite.height));
        fields.add_field_method_get("is_transparent", |_lua, sprite| Ok(sprite.is_transparent));
        fields.add_field_method_get("data", |_lua, sprite| Ok(sprite.data.clone()));
    }
}

impl Sprite {
    pub fn load_from_file<P>(
        path: P,
        attrs: &imagequant::Attributes,
        quantizer: &mut imagequant::QuantizationResult,
        dithering_level: f32
    ) -> Result<Sprite, LuaError>
    where
        P: AsRef<Path>,
    {
        let read = match image::io::Reader::open(&path)?
            .with_guessed_format()?
            .decode()
        {
            Ok(i) => i,
            Err(e) => {
                return Err(CompileError::Image(path.as_ref().to_path_buf(), e).into());
            }
        }
        .into_rgba8();

        let width = read.width() as usize;
        let height = read.height() as usize;
        if width > 255 || height > 255 {
            return Err(CompileError::ImageSize(path.as_ref().to_path_buf(), width, height).into());
        }

        let mut any_transparent = false;
        let buf: Vec<imagequant::RGBA> = read
            .pixels()
            .map(|p| {
                let mut channels = p.0;
                if channels[3] != 255 {
                    // TODO: warn on partial transparency
                    channels[3] = if channels[3] < 128 {
                        any_transparent = true;
                        0
                    } else {
                        255
                    }
                }
                imagequant::RGBA::from(channels)
            })
            .collect();

        let mut qimg = imagequant::Image::new(&attrs, buf, width, height, 0.)
            .expect("Failed to create imagequant image from loaded image");
        quantizer.set_dithering_level(dithering_level).unwrap();
        let (_palette, mut data) = quantizer
            .remapped(&mut qimg)
            .expect("Image remapping failed");

        // If the input image has transparency, ensure transparent pixels were set to the
        // palette index that marks transparency.
        if any_transparent {
            for (ipx, opx) in read.pixels().zip(data.iter_mut()) {
                if ipx.0[3] == 0 {
                    *opx = PaletteIndex::TRANSPARENT.0;
                }
            }
        }

        Ok(Sprite {
            is_transparent: any_transparent,
            width: width as u8,
            height: height as u8,
            data,
        })
    }
}

trait LuaImageExt {
    fn quantizer(&self) -> mlua::AppDataRefMut<imagequant::QuantizationResult>;
    fn attributes(&self) -> mlua::AppDataRefMut<imagequant::Attributes>;

    fn quantize_color(&self, rgba: [u8; 4]) -> PaletteIndex {
        if rgba[3] == 0 {
            return PaletteIndex::TRANSPARENT;
        }

        let mut quantizer = self.quantizer();
        let iqa = self.attributes();

        let pixel_buf = &[rgba.into()];
        let mut pixel_image = imagequant::Image::new_borrowed(&iqa, pixel_buf, 1, 1, 0.)
            .expect("Image::new_borrowed for single pixel failed");
        let mut remapped = [std::mem::MaybeUninit::zeroed()];
        quantizer
            .remap_into(&mut pixel_image, &mut remapped)
            .expect("remap_into for single pixel failed");

        // Safety: remap_into is guaranteed to initialize the buffer.
        PaletteIndex(unsafe { remapped[0].assume_init() })
    }
}

impl LuaImageExt for Lua {
    fn quantizer(&self) -> mlua::AppDataRefMut<imagequant::QuantizationResult> {
        self.app_data_mut::<imagequant::QuantizationResult>()
            .expect("imagequant::QuantizationResult app data not set")
    }

    fn attributes(&self) -> mlua::AppDataRefMut<imagequant::Attributes> {
        self.app_data_mut::<imagequant::Attributes>()
            .expect("imagequant::Attributes app data not set")
    }
}

fn load_global_palette() -> image::RgbaImage {
    static PALETTE_PNG: &[u8] = include_bytes!("../../src/gfx/palette_system_winvga_web216.png");

    image::load_from_memory_with_format(PALETTE_PNG, image::ImageFormat::Png)
        .expect("Failed to load internal palette PNG")
        .into_rgba8()
}

fn create_quantizer() -> (imagequant::Attributes, imagequant::QuantizationResult) {
    let palette = load_global_palette();

    let mut attrs = imagequant::new();
    attrs
        .set_max_colors(palette.width() * palette.height())
        .expect("Failed to set quantizer color count to palette size");

    let mut histogram = imagequant::Histogram::new(&attrs);

    for pixel in palette.pixels() {
        use image::Pixel;

        let mut channels = pixel.to_rgba().0;
        // Clamp alpha to fully opaque or fully transparent
        channels[3] = if channels[3] < 128 { 0 } else { 255 };

        histogram
            .add_fixed_color(imagequant::RGBA::from(channels), 0.0)
            .expect("Failed to add palette color ({x}, {y}) to histogram");
    }

    let result = histogram
        .quantize(&attrs)
        .expect("Failed to generate quantizer from histogram");

    (attrs, result)
}

fn lfn_load_sprite(lua: &Lua, (path, dithering_level): (String, f32)) -> LuaResult<Sprite> {
    Sprite::load_from_file(&path, &lua.attributes(), &mut lua.quantizer(), dithering_level)
}

fn lfn_quantize_color(_lua: &Lua, color: PaletteIndex) -> LuaResult<PaletteIndex> {
    Ok(color)
}

pub fn setup_lua(lua: &Lua, module: &LuaTable) -> LuaResult<()> {
    let (attrs, quant) = create_quantizer();
    lua.set_app_data(attrs);
    lua.set_app_data(quant);

    register_lua_function!(lua, module, "load_sprite", lfn_load_sprite);
    register_lua_function!(lua, module, "quantize_color", lfn_quantize_color);

    Ok(())
}
