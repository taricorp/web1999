use std::path::PathBuf;

use mlua::prelude::*;
use thiserror::Error;

macro_rules! register_lua_function {
    ($lua:expr, $table:expr, $name:literal, $impl:expr) => {
        $table.set($name, $lua.create_function($impl)?)?
    };
}

mod bytecode;
mod images;

pub use bytecode::BytecodeOutput;
use bytecode::BYTECODE_END;

#[derive(Error, Debug)]
pub enum CompileError {
    #[error("I/O error: {0}")]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    Lua(#[from] mlua::Error),
    #[error("Image {0} is too large: size may not exceed 255x255 but is {1}x{2}")]
    ImageSize(PathBuf, usize, usize),
    #[error("Unable to decode image {0}: {1}")]
    Image(PathBuf, #[source] image::error::ImageError),
}

impl Into<mlua::Error> for CompileError {
    fn into(self) -> LuaError {
        if let CompileError::Lua(e) = self {
            e
        } else {
            LuaError::ExternalError(std::sync::Arc::new(self))
        }
    }
}

pub fn execute<'a, T>(source: T) -> Result<BytecodeOutput, CompileError>
where
    for<'lua> T: mlua::AsChunk<'lua, 'a>,
{
    let lua = Lua::new();
    setup_env(&lua)?;

    // Load script and compile it, validating its syntax.
    let chunk = lua.load(source);
    let compiled = chunk.into_function()?;
    compiled.call::<_, LuaValue>(())?;

    let mut bc = lua.remove_app_data::<BytecodeOutput>().unwrap();
    // Ensure an EOF bytecode is present
    if !bc.codes.ends_with(&[BYTECODE_END]) {
        bc.codes.push(BYTECODE_END);
    }
    Ok(bc)
}

pub fn setup_env(lua: &Lua) -> LuaResult<()> {
    fn populate_table(lua: &Lua, _: ()) -> LuaResult<LuaTable> {
        let table = lua.create_table()?;
        bytecode::setup_lua(&lua, &table)?;
        images::setup_lua(&lua, &table)?;

        Ok(table)
    }

    let mod_init_fn = lua.create_function(populate_table)?;
    lua.load_from_function::<LuaTable>("_w9bcc", mod_init_fn)?;
    Ok(())
}
