require 'w9bcc'

local window = { width = 168, height = 105 }
WindowSize(window)
WindowTitle("Singles near you!")
FillWindow('white')

local image = { width = 96, height = 64 }
local broken_image_icon = Sprite:load('../src/gfx/broken_image.png')
DrawRectangle{ 2, 2, image.width, image.height }
broken_image_icon:draw_at{image.width / 2 - broken_image_icon.width / 2,
                          image.height / 2 - broken_image_icon.height / 2}

local wordart_hot = Sprite:load('../src/gfx/hotsingles_left.png')
local wordart_singles = Sprite:load('../src/gfx/hotsingles_right.png')
local wordart_width = wordart_hot.width + wordart_singles.width + 10
local wordart_top = image.height + 2
local wordart_left = (window.width - wordart_width) // 2
wordart_hot:draw_at{wordart_left, wordart_top}
wordart_singles:draw_at{wordart_left + wordart_hot.width + 10, wordart_top}

DrawText{"Waiting to meet today!", x = 10, y = 94}
SetTextColor{'green'}
DrawText{"CHAT", 118, 22}
DrawText{"NOW!", 120, 30}