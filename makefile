# ----------------------------
# Makefile Options
# ----------------------------

NAME = WEB1999
ICON = ie16.png
DESCRIPTION = "1999-style web browser"
COMPRESSED = YES
ARCHIVED = YES

EXTRA_DEFINES ?=
CFLAGS = -Wall -Wextra -Oz $(EXTRA_DEFINES)
CXXFLAGS = -Wall -Wextra -Oz $(EXTRA_DEFINES)

WINDOW_DEFS := HotSingles DragonballFanpage Demotivator NapsterVirus \
               HomestarRunner GotMilk MillionthVisitor AolMailer \
               PersonalityTest AllYourBase Ticalc
EXTRA_CXX_SOURCES = $(addprefix windows/,$(addsuffix .cpp,$(WINDOW_DEFS)))
EXTRA_HEADERS = windows/windows.h

CARGO = cargo $(1) --manifest-path $(abspath bcc/Cargo.toml)

EXTRA_CLEAN = $(MAKE) $(addprefix clean-,readme release)

GIT_VERSION := $(shell git describe --always --dirty)

# ----------------------------

include $(shell cedev-config --makefile)

RUN_BCC = cd $(dir $1) && $(call CARGO,run) --bin w99bcc -- \
			  --output-type=cxx-source --name $3 \
			  --print-stats \
			  $(notdir $1) $(abspath $2)

windows/%.cpp: windows/%.lua windows/w9bcc.lua
	$(call RUN_BCC,$<,$@,$*)

windows/%.cpp: windows/%/window.lua $(wildcard windows/%/*) windows/w9bcc.lua
	$(call RUN_BCC,$<,$@,$*)

windows/windows.h: $(EXTRA_CXX_SOURCES)
	echo > $@ && \
	for n in $(WINDOW_DEFS); do echo "WINTYPE($${n})" >> $@; done

.PHONY: windows clean-windows
windows: windows/windows.h $(EXTRA_CXX_SOURCES)
clean-windows:
	rm -f $(EXTRA_CXX_SOURCES) windows/windows.h

.PHONY: testvis
testvis:
	$(MAKE) debug NAME=W9TESTV DESCRIPTION="WEB1999 test visualizer" EXTRA_DEFINES=-DTESTVIS

.PHONY: web clean-web
web:
	$(MAKE) -C web build-cargo
	$(MAKE) -C web
clean-web:
	$(MAKE) -C web clean

README.html: README.md scripts/readme-extra.css $(wildcard img/*)
	pandoc -f markdown+auto_identifiers -t html \
		--shift-heading-level-by=-1 \
		--metadata=document-css=true --css scripts/readme-extra.css \
		--metadata=subtitle:"$(GIT_VERSION)" \
		--metadata=date:"$(shell date "+%B %-d, %Y")" \
		--metadata=toc-title=Contents \
		--self-contained --toc --toc-depth=2 $< -o $@
README.txt: README.md scripts/remove-images.lua
	pandoc -f markdown -t markdown \
		--reference-links --reference-location=section \
		--lua-filter=scripts/remove-images.lua \
		--markdown-headings=setext $< -o $@
.PHONY: readme clean-readme
readme: README.html README.txt
clean-readme:
	$(call RM,README.html README.txt)

WEB1999-$(GIT_VERSION).zip: bin/WEB1999.8xp README.html README.txt LICENSE.txt
	git archive --format=zip -o $@ $(addprefix --add-file=,$^) --prefix=source/ HEAD

.PHONY: release clean-release
release: WEB1999-$(GIT_VERSION).zip
clean-release:
	$(call RM,$(wildcard WEB1999-*.zip))

.PHONY: distclean clean-cargo
clean-cargo:
	$(call CARGO,clean)
distclean: clean clean-windows clean-cargo clean-web clean-release
